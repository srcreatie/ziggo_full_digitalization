function setElements(callback) {

    config = {};
    config.bannerWidth = 120;
    config.bannerHeight = 600;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('semibold.woff')
        ], add);
    }

    function add() {

        var textPosition = {};
        textPosition.topLeft = { left: 14, top: 20 }

        ___("frame1>f1_image")
            .image(asset("1.png"), { width: 246, height: 167, fit: true })
            .position({ left: 0, top: 270 })

        ___("frame1>h1")
            .text("Ben je <br />al mee-<br />geschakeld <br />naar <br />digitale kabel <br />tv?", { fontSize: 20, width: 110, webfont: "semibold", color: "#FF8D00" })
            .style({ css: "line-height:1", greensock: { rotation: -3 } })
            .position({ top: 35, left: 8 })

        ___("frame1>round1")
            .style({ css: "border:2px solid #566d6d; width:6px; height:6px; -webkit-border-radius:100%; -moz-border-radius:100%; border-radius:100%" })
            .position({ top: 250, right: 35 });

        ___("frame1>round2")
            .style({ css: "border:2px solid #566d6d; width:8px; height:8px; -webkit-border-radius:100%; -moz-border-radius:100%; border-radius:100%" })
            .position({ el: __("round1"), centerY: -10, push: { el: __("round1"), left: 3 } });

        ___("frame1>round3")
            .style({ css: "border:2px solid #566d6d; width:45px; height:45px; -webkit-border-radius:100%; -moz-border-radius:100%; border-radius:100%" })
            .position({ el: __("round2"), centerY: -15, push: { el: __("round2"), left: 5 } });

        ___("frame1>vt")
            .text("?", { fontSize: 32, width: 45, height: 45, webfont: "semibold", color: "#566d6d", textAlign: "center center" })
            .position({ el: __("round3"), centerX: 15, centerY: 0 });

        ___("frame2")
            .position({ left: config.bannerWidth })

        ___("frame2>f2_image")
            .image(asset("2.png"), { width: 152, height: 148, fit: true })
            .position({ left: 10, top: 220 })

        ___("frame3")
            .position({ left: config.bannerWidth })

        ___("frame3>f3_image")
            .image(asset("3.png"), { width: 246, height: 164, fit: true })
            .position({ left: 0, top: 273 })

        ___("frame3>check_background")
            .image(asset("check_background.png"), { fit: true, width: 40, height: 42 })
            .position({ left: 40, top: 301 })

        ___("frame3>check")
            .image(asset("check.png"), { fit: true, width: 19, height: 15 })
            .style({ css: "overflow:hidden" })
            .position({ left: 50, top: 316 })

        ___("h2")
            .text("Nog niet?", { fontSize: 28, width: 110, webfont: "semibold", color: "#FF8D00" })
            .style({ css: "line-height:1", greensock: { rotation: -3 } })
            .position({ top: 35, left: 8 })

        ___("h3")
            .text("Schakel dan nu mee naar digitale kabel tv", { fontSize: 24, width: 105, webfont: "semibold", color: "#ff8d00" })
            .style({ css: "line-height:1", greensock: { rotation: -3 } })
            .position({ top: 35, left: 8 });

        ___("fractal")
            .image(asset("fractal_sky_small.png"), { width: 120, height: 194, fit: true })
            .position({ bottom: 0, left: 0 })

        ___("cta")
            .text("Help mij <br />meeschakelen", { addClass: "cta", webfont: "semibold", fontSize: 14, color: "#fff" })
            .position({ centerX: 0, bottom: 90 });

        ___("tv_effect")
            .style({ background: "#000", width: config.bannerWidth, height: config.bannerHeight })
            .position({ left: 0, top: 0 });

        ___("tv_flash")
            .style({ width: config.bannerWidth, height: config.bannerHeight, background: "#fff" })
            .position({ centerX: 0, centerY: 0 })

        ___("logo")
            .image(asset("logo.png"), { width: 99, height: 57, fit: true })
            .position({ right: 0, bottom: 0 })

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {

    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {

        if (srBanner.dcs) {

            Enabler.exit('Background Exit');

        } else {

            window.open(clickTag);

        }

    }

}

module.exports.setElements = setElements;