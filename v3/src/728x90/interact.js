function animate(callback) {

    setAnimation();

    function setAnimation() {

        var tl01 = new TimelineMax({  });
        var tlRollover = new TimelineMax({ paused:true });

        var transformHeader = "center right";
        var frame1Group = [__("h1")];
        var frame2Group = [__("h2")];
        var frame3Group = [__("h3")];

        tl01.set(frame1Group, {alpha:0, rotation:-20, y:360, x:20, transformOrigin:transformHeader});
        tl01.set(frame2Group, {alpha:0, rotation:-20, y:360, x:20, transformOrigin:transformHeader});
        tl01.set(frame3Group, {alpha:0, rotation:-20, y:360, x:20, transformOrigin:transformHeader});

        tl01.set(__("tv_flash"), { scaleY:0 })
        tl01.set(__("creative"), { alpha:1 })

        tl01.to(__("tv_flash"), 0.3, { alpha:0, scaleY:1 })
        tl01.to(__("tv_effect"), 0.1, { alpha:0 }, "-=0.2")

        tl01.to(__("h1"), 1.4, { alpha:1, rotation:-3, y:0, x:0, transformOrigin:transformHeader, ease:Back.easeInOut.config(0.8)})
        tl01.staggerFrom([round1, round2, round3], 0.4, {scale:0, ease:Sine.easeOut}, "0.25");
        tl01.from(vt, 0.4, {scale:0, ease:Power3.easeOut, transformOrigin:"left center"}, "-=0.1");
        tl01.to(this, 0.5, {})

        // tl01.to(__("tv_flash"), 0.3, { alpha:1, scaleY:0 })
        // tl01.to(__("tv_effect"), 0.1, { alpha:1 }, "-=0.2")
        // tl01.to(this, 1.5, {})
        tl01.to(__("frame1"), 0.75, { y:-config.bannerHeight*2, ease:Back.easeInOut.config(1.2) }, "toSecond")
        tl01.to(__("frame2"), 0.75, { y:-config.bannerHeight, ease:Back.easeIn.config(1.2) }, "toSecond")
        // tl01.to(__("tv_effect"), 0.5, { alpha:0 })


        tl01.to(__("h2"), 1.4, { alpha:1, rotation:-3, y:0, x:0, transformOrigin:transformHeader, ease:Back.easeInOut.config(0.8)})
        tl01.to(this, 1, {})
        tl01.to([__("frame2"), __("h2")], 0.75, { y:-config.bannerHeight*2, alpha:0, ease:Back.easeIn.config(1.2) }, "toLast")
        tl01.to(__("frame3"), 0.75, { y:-config.bannerHeight, alpha:1, ease:Back.easeIn.config(1.2) }, "toLast")
        tl01.to(__("h3"), 1.4, { alpha:1, rotation:-3, y:0, x:0, transformOrigin:transformHeader, ease:Back.easeInOut.config(0.8)})

        tl01.from(__("check_background"), 0.5, { scale:0 })
        tl01.from(__("check"), 0.5, { width:0, transformOrigin:"center left" })

        tl01.to(__("cta"), 0.5, { scale:1.1, yoyo:true, repeat:1, onComplete:rollover, ease:Back.easeInOut.config(0.8) })

        tlRollover.to(this, 0.2, {})
        tlRollover.fromTo(__("check"), 0.5, { width:0 }, { width:25, transformOrigin:"center left" })
        tlRollover.to(__("cta"), 0.3, { scale:1.1, ease:Back.easeInOut.config(0.8), yoyo:true, repeat:1 })

        function rollover(){
            __("banner").onmouseover = function(){
                
                tlRollover.play(0);
            };

            __("banner").onmouseout = function(){
                
                // tlRollover.reverse();
            };
        }

        if (srBanner.debug) {

            if (srBanner.debug && srBanner.pauseFrom) {

                console.log("pause from " + srBanner.pauseFrom);
                tl01.pause(srBanner.pauseFrom);
            }

            if (srBanner.debug && srBanner.playFrom) {

                console.log("play from " + srBanner.playFrom);
                tl01.pause(srBanner.playFrom);
            }


            if (srBanner && srBanner.backupImage) {

                console.log("create backup images of last frame");
                tl01.add("screenshot");
                tl01.pause("screenshot");
            }
        }

    }

    if (callback) {

        callback();

    }

}

module.exports.animate = animate;