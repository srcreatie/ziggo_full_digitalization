function setElements(callback) {

    config = {};
    config.bannerWidth = 336;
    config.bannerHeight = 280;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('semibold.woff')
        ], add);
    }

    function add() {
        ___("frame1>f1_image")
            .image(asset("1.png"), { width: 185, height: 135, fit: true })
            .position({ left: 50, top: 75 });

        ___("frame1>h1")
            .text("Ben je al meegeschakeld naar digitale kabel tv?", { fontSize: 24, width: 275, webfont: "semibold", color: "#ff8d00" })
            .style({ css: "line-height:1", greensock: { rotation: -3 } })
            .position({ top: 5, left: 20 });

        ___("frame1>round1")
            .style({ css: "border:2px solid #566d6d; width:6px; height:6px; -webkit-border-radius:100%; -moz-border-radius:100%; border-radius:100%" })
            .position({ el: __("f1_image"), top: -10, push: { el: __("f1_image"), right: -8 } });

        ___("frame1>round2")
            .style({ css: "border:2px solid #566d6d; width:8px; height:8px; -webkit-border-radius:100%; -moz-border-radius:100%; border-radius:100%" })
            .position({ el: __("round1"), centerY: -5, push: { el: __("round1"), right: 3 } });

        ___("frame1>round3")
            .style({ css: "border:2px solid #566d6d; width:45px; height:45px; -webkit-border-radius:100%; -moz-border-radius:100%; border-radius:100%" })
            .position({ el: __("round2"), centerY: -2, push: { el: __("round2"), right: 5 } });

        ___("frame1>vt")
            .text("?", { fontSize: 32, width: 45, height: 45, webfont: "semibold", color: "#566d6d", textAlign: "center center" })
            .position({ el: __("round3"), centerX: 15, centerY: 0 });

        ___("frame2")
            .position({ left: config.bannerWidth });

        ___("frame2>f2_image")
            .image(asset("2.png"), { width: 166, height: 163, fit: true })
            .position({ left: 100, top: 50 });

        ___("frame3")
            .position({ left: config.bannerWidth });

        ___("frame3>f3_image")
            .image(asset("3.png"), { width: 276, height: 177, fit: true })
            .position({ left: 16, top: 60 });

        ___("frame3>check_background")
            .image(asset("check_background.png"), { fit: true, width: 46, height: 46 })
            .position({ left: 63, top: 92 });

        ___("frame3>check")
            .image(asset("check.png"), { fit: true, width: 25, height: 18 })
            .style({ css: "overflow:hidden" })
            .position({ left: 73, top: 106 });

        ___("h2")
            .text("Nog niet?", { fontSize: 24, width: 250, webfont: "semibold", color: "#ff8d00" })
            .style({ css: "line-height:1", greensock: { rotation: -3 } })
            .position({ top: 5, left: 20 });

        ___("h3")
            .text("Schakel dan nu mee naar digitale kabel tv", { fontSize: 24, width: 250, webfont: "semibold", color: "#ff8d00" })
            .style({ css: "line-height:1", greensock: { rotation: -3 } })
            .position({ top: 5, left: 20 });

        ___("fractal")
            .image(asset("fractal.png"), { width: 336, height: 133, fit: true })
            .position({ top: 165, left: 0 });

        ___("cta")
            .text("Help mij meeschakelen", { addClass: "cta", webfont: "semibold", fontSize: 16, color: "#fff" })
            .position({ left: 20, bottom: 20 });

        ___("tv_effect")
            .style({ background: "#000", width: config.bannerWidth, height: config.bannerHeight })
            .position({ left: 0, top: 0 });

        ___("tv_flash")
            .style({ width: config.bannerWidth, height: config.bannerHeight, background: "#fff" })
            .position({ centerX: 0, centerY: 0 })


        ___("logo")
            .image(asset("logo.png"), { width: 106, height: 61, fit: true })
            .position({ right: 0, bottom: 0 });

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTag);
        }
    }
}

module.exports.setElements = setElements;