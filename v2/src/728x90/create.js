function setElements(callback) {

    config = {};
    config.bannerWidth = 728;
    config.bannerHeight = 90;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('semibold.woff')
        ], add);
    }

    function add() {

        var textPosition = {};
        textPosition.topLeft = { left: 14, top: 20 }


        ___("fractal")
            .image(asset("fractal_landscape.png"), { width: 339, height: 370 })
            .position({ top: 0, right: 0 })

        ___("frame1>f1_image")
            .image(asset("1.png"), { width: 158, height: 107, fit: true })
            .position({ left: 226, top: -5 })

        ___("frame1>h1")
            .text("Schakel mee naar digitale kabel tv", { fontSize: 22, width: 206, webfont: "semibold", color: "#FF8D00" })
            .style({ css: "line-height:1", greensock: { rotation: -3 } })
            .position({ top: 19, left: 20 })

        ___("frame2")
            .position({ top: config.bannerHeight })

        ___("frame2>f2_image")
            .image(asset("2.png"), { width: 116, height: 114, fit: true })
            .position({ left: 252, top: -5 })

        ___("frame3")
            .style({ opacity: 0 })
            .position({ top: config.bannerHeight })

        ___("frame3>f3_image")
            .image(asset("3.png"), { width: 172, height: 111, fit: true })
            .position({ left: 205, top: 4 })

        ___("frame3>check_background")
            .image(asset("check_background.png"), { fit: true, width: 28, height: 28 })
            .position({ left: 229, top: 18 })

        ___("frame3>check")
            .image(asset("check.png"), { fit: true, width: 13, height: 10 })
            .style({ css: "overflow:hidden" })
            .position({ left: 234, top: 28 })

        ___("h2")
            .text("Check alle tv's in huis", { fontSize: 25, width: 195, webfont: "semibold", color: "#FF8D00" })
            .style({ css: "line-height:1", greensock: { rotation: -3 } })
            .position({ top: 15, left: 18 })

        ___("cta")
            .text("Ik wil meeschakelen", { addClass: "cta", webfont: "semibold", fontSize: 14, color: "#fff" })
            .position({ left: 470, bottom: 10 });

        ___("tv_effect")
            .style({ background: "#000", width: config.bannerWidth, height: config.bannerHeight })
            .position({ left: 0, top: 0 });

        ___("tv_flash")
            .style({ width: config.bannerWidth, height: config.bannerHeight, background: "#fff" })
            .position({ centerX: 0, centerY: 0 })

        ___("logo")
            .image(asset("logo.png"), { width: 99, height: 57, fit: true })
            .position({ right: 0, bottom: 0 })

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {

    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {

        if (srBanner.dcs) {

            Enabler.exit('Background Exit');

        } else {

            window.open(clickTag);

        }

    }

}

module.exports.setElements = setElements;