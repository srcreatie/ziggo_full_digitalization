function setElements(callback) {

    config = {};
    config.bannerWidth = 300;
    config.bannerHeight = 250;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('semibold.woff')
        ], add);
    }

    function add() {

        var textPosition = {};
        textPosition.topLeft = { left: 14, top: 20 }

        ___("frame1>f1_image")
            .image(asset("1.png"), { width: 236, height: 179, fit: true })
            .position({ left: 32, top: 50 })

        ___("frame1>h1")
            .text("Schakel mee naar digitale kabel tv", { fontSize: 21, width: 250, webfont: "semibold", color: "#ff8d00" })
            .style({ css: "line-height:1", greensock: { rotation: -3 } })
            .position({ top: 5, left: 20 })

        ___("frame2")
            .position({ left: config.bannerWidth })

        ___("frame2>f2_image")
            .image(asset("2.png"), { width: 243, height: 197, fit: true })
            .position({ left: 29, top: 46 })

        ___("frame3")
            .position({ left: config.bannerWidth })

        ___("frame3>f3_image")
            .image(asset("3.png"), { width: 266, height: 158, fit: true })
            .position({ left: 17, top: 46 })

        ___("frame3>check_background")
            .image(asset("check_background.png"), { fit: true, width: 31, height: 43 })
            .position({ left: 53, top: 66 })

        ___("frame3>check")
            .image(asset("check.png"), { fit: true, width: 18, height: 18 })
            .style({ css: "overflow:hidden" })
            .position({ left: 59, top: 79 })

        ___("h2")
            .text("Check alle tv's in huis", { fontSize: 24, width: 250, webfont: "semibold", color: "#ff8d00" })
            .style({ css: "line-height:1", greensock: { rotation: -3 } })
            .position({ top: 5, left: 20 })

        ___("fractal")
            .image(asset("fractal.png"), { width: 300, height: 119 })
            .position({ top: 165, left: 0 })

        ___("cta")
            .text("Ik wil meeschakelen", { addClass: "cta", webfont: "semibold", fontSize: 14, color: "#fff" })
            .position({ left: 20, bottom: 10 });

        ___("tv_effect")
            .style({ background: "#000", width: config.bannerWidth, height: config.bannerHeight })
            .position({ left: 0, top: 0 });

        ___("tv_flash")
            .style({ width: config.bannerWidth, height: config.bannerHeight, background: "#fff" })
            .position({ centerX: 0, centerY: 0 })

        ___("logo")
            .image(asset("logo.png"), { width: 95, height: 54, fit: true })
            .position({ right: 0, bottom: 0 })

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {

    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {

        if (srBanner.dcs) {

            Enabler.exit('Background Exit');

        } else {

            window.open(clickTag);

        }

    }

}

module.exports.setElements = setElements;