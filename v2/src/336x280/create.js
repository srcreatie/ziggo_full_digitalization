function setElements(callback) {

    config = {};
    config.bannerWidth = 336;
    config.bannerHeight = 280;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('semibold.woff')
        ], add);
    }

    function add() {

        var textPosition = {};
        textPosition.topLeft = { left: 14, top: 20 }

        ___("frame1>f1_image")
            .image(asset("1.png"), { width: 225, height: 153, fit: true })
            .position({ left: 57, top: 58 })

        ___("frame1>h1")
            .text("Schakel mee naar digitale kabel tv", { fontSize: 24, width: 250, webfont: "semibold", color: "#ff8d00" })
            .style({ css: "line-height:1", greensock: { rotation: -3 } })
            .position({ top: 5, left: 20 })

        ___("frame2")
            .position({ left: config.bannerWidth })

        ___("frame2>f2_image")
            .image(asset("2.png"), { width: 206, height: 203, fit: true })
            .position({ left: 68, top: 48 })

        ___("frame3")
            .position({ left: config.bannerWidth })

        ___("frame3>f3_image")
            .image(asset("3.png"), { width: 276, height: 177, fit: true })
            .position({ left: 44, top: 68 })

        ___("frame3>check_background")
            .image(asset("check_background.png"), { fit: true, width: 46, height: 46 })
            .position({ left: 82, top: 90 })

        ___("frame3>check")
            .image(asset("check.png"), { fit: true, width: 25, height: 18 })
            .style({ css: "overflow:hidden" })
            .position({ left: 90, top: 104 })

        ___("h2")
            .text("Check alle tv's in huis", { fontSize: 26, width: 250, webfont: "semibold", color: "#ff8d00" })
            .style({ css: "line-height:1", greensock: { rotation: -3 } })
            .position({ top: 5, left: 20 })

        ___("fractal")
            .image(asset("fractal.png"), { width: 336, height: 133, fit: true })
            .position({ top: 165, left: 0 })

        ___("cta")
            .text("Ik wil meeschakelen", { addClass: "cta", webfont: "semibold", fontSize: 16, color: "#fff" })
            .position({ left: 20, bottom: 20 });

        ___("tv_effect")
            .style({ background: "#000", width: config.bannerWidth, height: config.bannerHeight })
            .position({ left: 0, top: 0 });

        ___("tv_flash")
            .style({ width: config.bannerWidth, height: config.bannerHeight, background: "#fff" })
            .position({ centerX: 0, centerY: 0 })

        ___("logo")
            .image(asset("logo.png"), { width: 106, height: 61, fit: true })
            .position({ right: 0, bottom: 0 })

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {

    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {

        if (srBanner.dcs) {

            Enabler.exit('Background Exit');

        } else {

            window.open(clickTag);

        }

    }

}

module.exports.setElements = setElements;