function setElements(callback) {

    config = {};
    config.bannerWidth = 300;
    config.bannerHeight = 600;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('semibold.woff')
        ], add);
    }

    function add() {

        var textPosition = {};
        textPosition.topLeft = { left: 14, top: 20 }

        ___("frame1>f1_image")
            .image(asset("1.png"), { width: 246, height: 167, fit: true })
            .position({ left: 29, top: 219 })

        ___("frame1>h1")
            .text("Het analoge tv-signaal stopt echt", { fontSize: 41, width: 240, webfont: "semibold", color: "#FF8D00" })
            .style({ css: "line-height:1", greensock: { rotation: -3 } })
            .position({ top: 50, left: 30 })

        ___("frame2")
            .position({ left: config.bannerWidth })

        ___("frame2>f2_image")
            .image(asset("2.png"), { width: 203, height: 198, fit: true })
            .position({ left: 70, top: 160 })

        ___("frame3")
            .position({ left: config.bannerWidth })

        ___("frame3>f3_image")
            .image(asset("3.png"), { width: 246, height: 164, fit: true })
            .position({ left: 28, top: 226 })

        ___("frame3>check_background")
            .image(asset("check_background.png"), { fit: true, width: 40, height: 42 })
            .position({ left: 68, top: 256 })

        ___("frame3>check")
            .image(asset("check.png"), { fit: true, width: 19, height: 15 })
            .style({ css: "overflow:hidden" })
            .position({ left: 78, top: 271 })

        ___("h2")
            .text("Schakel mee naar digitale kabel tv", { fontSize: 34, width: 240, webfont: "semibold", color: "#FF8D00" })
            .style({ css: "line-height:1", greensock: { rotation: -3 } })
            .position({ top: 55, left: 32 })

        ___("fractal")
            .image(asset("fractal_sky.png"), { width: 300, height: 246, fit: true })
            .position({ bottom: 0, right: 0 })

        ___("cta")
            .text("Doe het nu", { addClass: "cta", webfont: "semibold", fontSize: 22, color: "#fff" })
            .position({ centerX: 0, bottom: 125 });

        ___("tv_effect")
            .style({ background: "#000", width: config.bannerWidth, height: config.bannerHeight })
            .position({ left: 0, top: 0 });

        ___("tv_flash")
            .style({ width: config.bannerWidth, height: config.bannerHeight, background: "#fff" })
            .position({ centerX: 0, centerY: 0 })

        ___("logo")
            .image(asset("logo.png"), { width: 143, height: 82, fit: true })
            .position({ right: 0, bottom: 0 })

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {

    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {

        if (srBanner.dcs) {

            Enabler.exit('Background Exit');

        } else {

            window.open(clickTag);

        }

    }

}

module.exports.setElements = setElements;