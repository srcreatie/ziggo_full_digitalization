function setElements(callback) {

    config = {};
    config.bannerWidth = 970;
    config.bannerHeight = 250;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('semibold.woff')
        ], add);
    }

    function add() {

        var textPosition = {};
        textPosition.topLeft = { left: 14, top: 20 }

        ___("fractal")
            .image(asset("fractal_landscape.png"), { width: 435, height: 475 })
            .position({ top: 0, right: 0 })

        ___("frame1>f1_image")
            .image(asset("1.png"), { width: 318, height: 216, fit: true })
            .position({ left: 113, bottom: 0 })

        ___("frame1>h1")
            .text("Het analoge tv-signaal stopt echt", { fontSize: 36, width: 380, webfont: "semibold", color: "#ffffff" })
            .style({ css: "line-height:1", greensock: { rotation: -3 } })
            .position({ top: 65, left: 575 })

        ___("frame2")
            .position({ top: config.bannerHeight })

        ___("frame2>f2_image")
            .image(asset("2.png"), { width: 203, height: 198, fit: true })
            .position({ left: 173, top: 35 })

        ___("frame3")
            .style({ opacity: 0 })
            .position({ top: config.bannerHeight })

        ___("frame3>f3_image")
            .image(asset("3.png"), { width: 348, height: 226, fit: true })
            .position({ left: 123, top: 39 })

        ___("frame3>check_background")
            .image(asset("check_background.png"), { fit: true, width: 55, height: 55 })
            .position({ left: 178, top: 78 })

        ___("frame3>check")
            .image(asset("check.png"), { fit: true, width: 25, height: 18 })
            .style({ css: "overflow:hidden" })
            .position({ left: 192, top: 97 })

        ___("h2")
            .text("Schakel mee naar digitale kabel tv", { fontSize: 36, width: 380, webfont: "semibold", color: "#fff" })
            .style({ css: "line-height:1", greensock: { rotation: -3 } })
            .position({ top: 65, left: 575 })


        ___("cta")
            .text("Doe het nu", { addClass: "cta", webfont: "semibold", fontSize: 22, color: "#fff" })
            .position({ left: 575, bottom: 20 });

        ___("tv_effect")
            .style({ background: "#000", width: config.bannerWidth, height: config.bannerHeight })
            .position({ left: 0, top: 0 });

        ___("tv_flash")
            .style({ width: config.bannerWidth, height: config.bannerHeight, background: "#fff" })
            .position({ centerX: 0, centerY: 0 })

        ___("logo")
            .image(asset("logo.png"), { width: 143, height: 82, fit: true })
            .position({ right: 0, bottom: 0 })

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {

    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {

        if (srBanner.dcs) {

            Enabler.exit('Background Exit');

        } else {

            window.open(clickTag);

        }

    }

}

module.exports.setElements = setElements;