<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Searchresult Preview pagina</title>
    <link href="https://fonts.googleapis.com/css?family=Exo+2:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
  </head>
  
  <body>
  <div id="header">
    <div class="project">
      <?php getProjectFolder(); ?>
  </div>

  <?php getProjectItems(); ?>

  <div id="main-content">
    <div class="content">
      <h1 class="none"><i class="fa fa-chevron-up" aria-hidden="true"></i></br>Selecteer een uiting</h1>      
    </div>
  </div>
  </body>

<?php
  function getProjectFolder(){
    // Check if the JsON file exists and if data is provided
    $jsonFile = 'srPreview.json';
    
    if(file_exists($jsonFile)){
      $jsonFile = 'srPreview.json';
      $projectInfo = file_get_contents($jsonFile);
      $json = json_decode($projectInfo);      

      // Get folder date
      $projectData = date ("d-m-Y", filemtime(glob('*')[0]));
      if($json->client_name){
        echo '<h1 class="'.$json->client_name.'">'.strtoupper($json->client_name).' <span class="project-title">- '.$json->project_name.'</span></h1>';
        echo ' <p>Preview pagina / '.$projectData.'</p></div>';
        echo '<a href="https://searchresult.nl" target="_blank"><img class="main-logo SR" src="https://adeptiv.nl/wp-content/uploads/2017/12/dvdp_adeptiv_logo_web_gif_no_subline_v2.gif" alt="Searchresult" border="0"/></a>';  
      } else {
        echo '<h1 class="client_name">Creative preview</h1>';
        echo ' <p>Preview / '.$projectData.'</p></div>';
        echo '<a href="https://searchresult.nl" target="_blank"><img class="main-logo SR" src="https://adeptiv.nl/wp-content/uploads/2017/12/dvdp_adeptiv_logo_web_gif_no_subline_v2.gif" alt="Searchresult" border="0"/></a>';
      }
    } else {
      $projectData = date ("d-m-Y", filemtime(glob('*')[0]));
      echo '<h1 class="client_name">Creative preview</h1><p>Preview / '.$projectData.'</p></div><a href="http://searchresult.nl" target="_blank"><img class="main-logo SR" src="http://sr-creatie.nl/preview_page-assets/images/sr-logo.png" alt="Searchresult" border="0"/></a>';
    } 
  }

  function getProjectItems(){
    $localhostList = array(      
      '127.0.0.1',
      '::1'
    );  

    $jsonFile = 'srPreview.json';

    // Check if the JsON file exists and if data is provided
    if(file_exists($jsonFile)){    
      $projectInfo = file_get_contents($jsonFile);
      $json = json_decode($projectInfo);      
      $excludes = $json->excludeFolders;

      if($json->hasSubfolders === true){
        $directories = glob($somePath . '*' , GLOB_ONLYDIR);
        arsort($directories);

        echo '<div id="content-menu"><ul class="content-items">';
        foreach ($directories as $project => $part) {
            $partStriped = str_replace('_', ' ', $part);
             if(!in_array($partStriped, $excludes)){
              echo '<li class="item" id="'.$part.'"><i class="fa fa-html5" aria-hidden="true"></i> '.$partStriped.'</li>';
            }
        }
  
        echo '</div></div><div id="sub-menu">';
        foreach ($directories as $project => $partStriped) {
          echo '<ul class="sub-items" id="'.$partStriped.'">';

          $projectPath = $partStriped;
          $directories = array();

          $projectDir = dir($projectPath);
  
          while (false !== ($entry = $projectDir->read())) {
            if ($entry != '.' && $entry != '..') {
               if (is_dir($projectPath . '/' .$entry)) {
                    $directories[] = $entry; 
               }
            }
          }

          arsort($directories);
      
          foreach($directories as $dir){  
            if(!in_array($dir, $excludes)){          
              echo '<li class="'.$dir.'">'.$dir.'</li>';
            }
          }
          echo '</ul>';
      }
        echo '</div>';

      } else {
        $directories = glob($somePath . '*' , GLOB_ONLYDIR);
        arsort($directories);

        echo '<div id="content-menu" class="no-subs"><ul class="content-items">';
          foreach ($directories as $banner) {
            if(!in_array($banner, $excludes)){
              echo '<li class="item" id="'.$banner.'"><i class="fa fa-html5" aria-hidden="true"></i> '.$banner.'</li>';              
            }
          }

          if(in_array($_SERVER['REMOTE_ADDR'], $localhostList)){
            echo '<li class="item" id="viewAll"><i class="fa fa-html5" aria-hidden="true"></i> view all</li>';
          }
        echo '</ul></div>';
      }
    } else {
      $directories = glob($somePath . '*' , GLOB_ONLYDIR);
        arsort($directories);

        echo '<div id="content-menu" class="no-subs"><ul class="content-items">';
          foreach ($directories as $banner) {
            if(!in_array($banner, $excludes)){
              echo '<li class="item" id="'.$banner.'"><i class="fa fa-html5" aria-hidden="true"></i> '.$banner.'</li>';
            }
          }
        
          if(in_array($_SERVER['REMOTE_ADDR'], $localhostList)){
            echo '<li class="item" id="viewAll"><i class="fa fa-html5" aria-hidden="true"></i> view all</li>';
          }
        echo '</ul></div>';
    }      
  }
?>

<style>
  


  /* Main css */
  body{
    width:100%;
    height:100%;
    padding:0;
    margin:0;
    background:#FFFFFF;
    text-rendering: optimizeLegibility !important;
    -webkit-font-smoothing:antialiased;
  }

  * { box-sizing:border-box; -webkit-box-sizing:border-box; -moz-box-sizing:border-box; }
  * { -webkit-transition:all 0.2s ease; -moz-transition:all 0.2s ease; -ms-transition:all 0.2s ease; -o-transition:all 0.2s ease; }

  /* Declaring Type Styles */ 
  h1, h2, h3, p, a, ul, li{ padding:0; margin:0; text-decoration:none; line-height:100%; list-style:none; border:0px;}

  div#header {
    width:100%;
    height:100px;
    background:#303030;
    padding:0 20px;
    border-bottom:1px solid #585858;
  }

  div#header div.project {
    color:#FFFFFF;
    width:56%;
    height:auto;
    display:inline-block;
  }

  div#header div.project h1 {
    /*font-family:proxima_novabold;*/
    font-family: 'Exo 2', sans-serif;
    font-weight: 700;
    font-size:34px;
    padding:30px 0 0 0;
  }
  div#header div.project h1 span.project-title {
    /*font-family:proxima_novaregular;*/
    font-family: 'Exo 2', sans-serif;
    font-weight: 400;
    font-size:34px;
  }

  div#header div.project p {
    /*font-family:proxima_novalight;*/
    font-family: 'Exo 2', sans-serif;
    font-weight: 300;
    font-size:14px;
    text-transform:uppercase;
    line-height:20px;
  }

  div#header img.main-logo.DU {
    float:right;
    margin-top:30px;
  }

  div#header img.main-logo.SR {
    float:right;
    margin-top:20px;
    width:auto;
    height:60px;
  }

  div#content-menu {
    width:100%;
    height:auto;
    padding:25px 25px 15px 25px;
    background:#303030;
    border-bottom:1px solid #212121;
    -webkit-box-shadow: 0px 1px 4px 0px rgba(0,0,0,0.5);
    -moz-box-shadow: 0px 1px 4px 0px rgba(0,0,0,0.5);
    box-shadow: 0px 1px 4px 0px rgba(0,0,0,0.5);
    z-index:5;
    position:relative;
  }

  div#content-menu ul.content-items,
  div#sub-menu ul.sub-items {
    list-style:none;
    display:table;
    margin:0 auto;
    text-align:center;
  }

  div#content-menu ul.content-items li.item,
  div#sub-menu ul.sub-items li {
    padding:15px 20px;
    background:#303030;
    color:#D4D4D4;
    text-align:center;
    display:inline-block;
    border-radius:8px 8px 8px 8px;
    -moz-border-radius:8px 8px 8px 8px;
    -webkit-border-radius:8px 8px 8px 8px;
    border:0px solid #000000;
    margin:0 15px 10px 0;
    /*font-family:proxima_novaregular;*/
    font-family: 'Exo 2', sans-serif;
    font-weight: 400;
    font-size:22px;
  }

  div#sub-menu ul.sub-items li {
    /*font-family:proxima_novalight;*/
    font-family: 'Exo 2', sans-serif;
    font-weight: 300;
    font-size:18px;
  }

  div#content-menu ul.content-items li.item.active,
  div#sub-menu ul.sub-items li.active {
    color:#fbdc01;
  }

  div#content-menu ul.content-items li.item:first-of-type,
  div#sub-menu ul.sub-items li:first-of-type {
    margin-left:0;
  }

  div#content-menu ul.content-items li.item+active,
  div#sub-menu ul.sub-items li.active {
    padding:15px 20px;
    background:#303030;
    color:#fbdc01;
  }

  div#content-menu ul.content-items li.item:hover,
  div#sub-menu ul.sub-items li:hover {
    cursor:pointer;
    color:#fbdc01;
  }

  div#sub-menu,
  div#sub-menu > ul.sub-items {
    display:none;
    padding:10px;
    background:#484848;
  }

  div#sub-menu ul.sub-items {
    /*background:#A5A5A5;*/
    margin:0 auto;
    padding-bottom:0px;
  }

  div#sub-menu ul.sub-items li {
    display:inline-block;
  }

  div#main-content {
    width:100%;
    height:auto;
    position:relative;
  }

  div#main-content div.left-menu {
    position:absolute;
    top:20px;
    left:0;
    width:300px;
    z-index:9999;
  }

  div#main-content div.left-menu div.reload-btn a,
  div#main-content div.left-menu div.options div.options-btn a {
    width:40px;
    height:40px;
    background:#D4D4D4;
    border-radius:0px 8px 8px 0px;
    -moz-border-radius:0px 8px 8px 0px;
    -webkit-border-radius:0px 8px 8px 0px;
    border:0px solid #000000;
    text-align:center;
    font-size:24px;
    line-height:44px;
    display:block;
  }

  .btn-box-shadow {
    -moz-box-shadow:0 4px #303030;
      -webkit-box-shadow:0 4px #303030;
    box-shadow:0 4px #303030;
  }

  div#main-content div.left-menu div.reload-btn a:active,
  div#main-content div.left-menu div.options div.options-btn a:active {
    /*background-color: #a5a5a5;*/
    box-shadow: 0 2px #303030;
    -moz-box-shadow:0 2px #303030;
    -webkit-box-shadow:0 2px #303030;
    -webkit-transform:translateY(3px);
    -ms-transform:translateY(3px);
    transform:translateY(3px);
  }

  div#main-content div.left-menu div.options div.options-btn a {
    float:left;
  }

  div#main-content div.left-menu div.options {
    margin-top:10px;
    margin-left:-200px; 
  }

  div#main-content div.left-menu div.reload-btn a i,
  div#main-content div.left-menu div.options div.options-btn a i {
    color:#303030;
      -webkit-transition: -webkit-transform .8s ease-in-out;
      transition: transform .3s ease-in-out;
  }

  div#main-content div.left-menu div.reload-btn a:hover i {
    color:#00C1FF;
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
    -ms-transform: rotate(360deg);
  }

  div#main-content div.left-menu div.options div.options-btn a:hover i {
    color:#00C1FF;
  }

  div#main-content div.left-menu div.options div.options-btn a i.fa.fa-chevron-left {
    display:none;
  }

  div#main-content div.left-menu div.options div.options-items {
    width:200px;
    background:#D4D4D4;
    border-radius:0px 0px 8px 0px;
    -moz-border-radius:0px 0px 8px 0px;
    -webkit-border-radius:0px 0px 8px 0px;
    border:0px solid #000000;
    float:left;
    padding:10px 0 10px 10px;
  }

  div#main-content div.left-menu div.options div.options-items h2 {
    /*font-family:proxima_novabold;*/
    font-family: 'Exo 2', sans-serif;
    font-weight: 700;
    font-size:18px;
    color:#303030;
    text-transform:uppercase;
  }

  div#main-content div.left-menu div.options div.options-items ul {
    display:inline-block;
  }

  div#main-content div.left-menu div.options div.options-items li {
    display:block;
    margin-top:10px;
    padding:0 10px 10px 0;
  }

  .reload{
    float: right;
    color: ##303030;
    cursor: pointer;
  }

  div#main-content div.left-menu div.options div.options-items li input,
  div#main-content div.left-menu div.options div.options-items li p {
    float:left;
  }

  div#main-content div.left-menu div.options div.options-items li input {
    margin:0 10px 0 0;
  }

  div#main-content div.left-menu div.options div.options-items li p {
    /*font-family:proxima_novaregular;*/
    font-family: 'Exo 2', sans-serif;
    font-weight: 400;
    font-size:14px;
  }

  div#main-content div.content {
    width:1048px;
    height:100%;
    margin:0 auto;
    padding-top:30px;
    text-align:center;
  }

  div#main-content div.content h1.none i.fa.fa-chevron-up {
    position:absolute;
    left:50%;
    bottom:0px;
    margin-top:-25px;
    margin-left:-25px;
    height:50px;
    width:50px;
    -webkit-animation:bounce 1s infinite;
    -moz-animation:bounce 1s infinite;
    -o-animation:bounce 1s infinite;
    animation:bounce 1s infinite;
  }

  .framebox{
    display: inline-block;
  }

  div#main-content div.content p.sub-details {
    /*font-family: proxima_novaregular;*/
    font-family: 'Exo 2', sans-serif;
    font-weight: 400;
    font-size: 14px;
    color: #303030;
    text-align: left;
    font-weight: normal;
    margin-bottom: 10px;
    border-bottom: 1px dashed #303030;
    padding-bottom: 5px;

  }

  @-webkit-keyframes bounce {
    0%       { bottom:15px; }
    25%, 75% { bottom:30px; }
    50%      { bottom:35px; }
    100%     {bottom:15px;}
  }

  @keyframes bounce {
    0%       { bottom:15px; }
    25%, 75% { bottom:30px; }
    50%      { bottom:35px; }
    100%     {bottom:15px;}
  }

  @-o-keyframes bounce {
    0%       { bottom:15px; }
    25%, 75% { bottom:30px; }
    50%      { bottom:35px; }
    100%     {bottom:15px;}
  }

  div#main-content div.content h1.none {
    /*font-family:proxima_novabold;*/
    font-family: 'Exo 2', sans-serif;
    font-weight: 700;
    font-size:32px;
    text-align:center;
    text-transform:uppercase;
    color:#D4D4D4;
  }

  @media screen and (max-width:1048px){
    div#main-content div.content {
      width:auto;
    }
  }

  @media screen and (max-width:970px){
    div#main-content div.content {
      padding-top:50px;
    }
  }

  @media screen and (max-width:740px){
    div#header {
      height:auto;
      padding-bottom:20px;
    }

    div#header img.main-logo.DU,
    div#header img.main-logo.SR {
      float:none;
      margin:0 auto;
      display:block;
      margin-top:10px;
    }

    div#header div.project {
      margin:0 auto;
      display:table;
      text-align:center;
    }

    div#header div.project h1 {
      padding-top:20px;
    }
  }

  @media screen and (max-width:530px){
    div#header div.project h1,
    div#header div.project h1 span.project-title {
      font-size:24px;
    }

    div#header div.project p {
      font-size:12px;
    }

    div#main-content div.content h1.none {
      font-size:17px;
    }

    div#main-content div.content h1.none i.fa.fa-chevron-up {
      width:25px;
      height:20px;
    }
  }
</style>

<script>
  $(document).ready(function(){
    // Declare variables
    var formatSelected = false,
    bannerDimension = '';

    if($('div#content-menu ul.content-items li').size() <= 1 ){
      $('div#content-menu ul.content-items li.item').css('margin-right', '0px');
    }

    if($('div#sub-menu ul.sub-items li').size() <= 1 ){
      $('div#sub-menu ul.sub-items li').css('margin-right', '0px');
    }

    $(window).on('resize', function(){
        if($(window).width() < 1000) {
            $('div#main-content div.left-menu div.options').hide();
        } else {
          $('div#main-content div.left-menu div.options').show();
        }
    });


    // Select menu item
    if($('div#content-menu').hasClass('no-subs')){        
      // If there's no banner selected, auto-select the first format in the menu
      if(formatSelected === false){
        $('div#content-menu li:first-child').addClass('active');
        var format = $('div#content-menu li:first-child').attr('id');                

        $('div#main-content div.content').html('<div class="framebox"><p class="sub-details"><i class="fa reload fa-repeat" aria-hidden="true"></i> '+format+'</p><iframe class="'+format+'" src="'+format+'" onload="javascript:resizeIframe(this);" frameborder="0" scrolling="no" id="myFrame"></iframe></div>');

        formatSelected = true;
      }

    if(window.location.href.indexOf("showall") > -1) {
          var formatArray = [];
            $("#content-menu ul").find(".item").each(function(){ if(this.id != 'viewAll'){formatArray.push(this.id); }});

            $('div#main-content div.content').html('');

            for(var i = 0; i < formatArray.length;){
              $('div#main-content div.content').append('<div class="viewAllFormat" style="float:left; width:auto; height:auto; padding:20px;"><div class="framebox"><p class="sub-details"><i class="fa reload fa-refresh" aria-hidden="true"></i>'+formatArray[i]+'</p><iframe class="'+formatArray[i]+'" src="'+formatArray[i]+'" onload="javascript:resizeIframe(this);" frameborder="0" scrolling="no" id="myFrame"></iframe></div></div>');
              i++;
              // alert();
            }        

    }


      $('div#content-menu ul.content-items li.item').on('click', function(){        
        if($(this).hasClass('active')){
          // Do nothing, it's already selected 
        } else {
            $(this).siblings().removeClass('active');
            $(this).addClass('active');

          if($(this).attr('id') === 'viewAll'){
            var formatArray = [];
            $("#content-menu ul").find(".item").each(function(){ if(this.id != 'viewAll'){formatArray.push(this.id); }});

            $('div#main-content div.content').html('');

            for(var i = 0; i < formatArray.length;){
              $('div#main-content div.content').append('<div class="viewAllFormat" style="float:left; width:auto; height:auto; padding:20px;"><div class="framebox"><p class="sub-details">'+formatArray[i]+'</p><iframe class="'+formatArray[i]+'" src="'+formatArray[i]+'" onload="javascript:resizeIframe(this);" frameborder="0" scrolling="no" id="myFrame"></iframe></div></div>');
              i++;
            }            

          } else {
            // $(this).siblings().removeClass('active');
            // $(this).addClass('active');

            var format = $(this).attr('id'); 

            $('div#main-content div.content').html('<div class="framebox"><p class="sub-details"><i class="fa reload fa-repeat" aria-hidden="true"></i> '+format+'</p><iframe class="'+format+'" src="'+format+'" onload="javascript:resizeIframe(this);" frameborder="0" scrolling="no" id="myFrame"></iframe></div>');               
          }
        }          
      });
    } else {
      $('div#content-menu ul.content-items li.item').on('click', function(){
      var activeItem = $(this).text().trim().replace(/ /g,"_");

      // If selected item is already active
      if($(this).hasClass('active')){
        $(this).removeClass('active');
        $('div#sub-menu ul#'+activeItem).hide();
        $('div#sub-menu').hide();
        $('div#main-content div.content h1.none').show();
      } else { // Else remove active class from siblings
        if($(this).siblings().hasClass('active')){
          // Remove and add active classes      
          $(this).siblings().removeClass('active');
          $('div#sub-menu ul.sub-items').removeClass('active');

          $(this).addClass('active');
          $('div#sub-menu ul.sub-items.'+activeItem).addClass('active');

          // Hide current menu if active and show new one
          $('div#sub-menu ul.sub-items').hide();
          $('div#sub-menu ul#'+activeItem).css({'display':'table'});

          // formatSelected = false;
          // // If there's no banner selected, auto-select the first format in the menu
          // if(formatSelected === false){

            $('div#sub-menu ul#'+activeItem+' li:first-child').addClass('active');

            var folderRoot = $('div#content-menu ul.content-items').find('li.active').attr('id'),
            folder = $(this).attr('id'),
            format = $('div#sub-menu ul#'+activeItem+' li:first-child').text(),
            projectName = $('div#header div.project h1').text();

            $('div#main-content div.content').html('<div class="framebox"><p class="sub-details"><i class="fa reload fa-repeat" aria-hidden="true"></i> '+format+'</p><iframe class="'+format+'" src="'+folderRoot+'/'+format+'" onload="javascript:resizeIframe(this);" frameborder="0" scrolling="no" id="myFrame"></iframe></div>');
          // }
              formatSelected = true;
        } else {
          $(this).addClass('active');
          $('div#sub-menu ul.sub-items.'+activeItem).addClass('active');

          // Hide hinting if still visible
          $('div#main-content div.content h1.none').hide();

          $('div#sub-menu').show(); 
            $('div#sub-menu ul#'+activeItem).css({'display':'table'});
            
            if(formatSelected === false){
              $('div#sub-menu ul#'+activeItem+' li:first-child').addClass('active');

              var folderRoot = $('div#content-menu ul.content-items').find('li.active').attr('id'),
              folder = $(this).attr('id'),
              format = $('div#sub-menu ul#'+activeItem+' li:first-child').text(),
              projectName = $('div#header div.project h1').text();

              $('div#main-content div.content').html('<div class="framebox"><p class="sub-details"><i class="fa reload fa-repeat" aria-hidden="true"></i> '+format+'</p><iframe class="'+format+'" src="'+folderRoot+'/'+format+'" onload="javascript:resizeIframe(this);" frameborder="0" scrolling="no" id="myFrame"></iframe></div>');
          }
            formatSelected = true;
        }
      }
    });

    // Select bannerformat
    $('div#sub-menu ul.sub-items li').on('click', function(){      
      var folderRoot = $('div#content-menu ul.content-items').find('li.active').attr('id'),
      folder = $(this).parent().attr('id'),
      format = $(this).text();

      var projectName = $('div#header div.project h1').text();

      $(this).siblings().removeClass('active');
      $(this).parent().siblings().children('li').removeClass('active');
      $(this).addClass('active');

      $('div#main-content div.content').html('<div class="framebox"><p class="sub-details"><i class="fa reload fa-repeat" aria-hidden="true"></i> '+format+'</p><iframe class="'+format+'" src="'+folderRoot+'/'+format+'" onload="javascript:resizeIframe(this);" frameborder="0" scrolling="no" id="myFrame"></iframe></div>');
      });
    }

     // Reload banner / iframe

    $(document).on('click', ".reload", function(){
      // console.log(formatSelected);
      if(formatSelected === true){
        // console.log(formatSelected + "in if");
        reloadIframe();
      }
    });

  });

  // Get iframe / banner dimensions and set styles
  window.resizeIframe = function(obj) {
     $.getJSON('srPreview.json', function(key, value) {
      if(key['parentDivId'].length === 0){
        // No parentDiv id given
        var myIframe = $(obj).contents().find("body");
        if(myIframe.length !== 0){
          var h = myIframe.height(),
          w = myIframe.width();

          if(key['parentDivSolidBorder'] === true){
            obj.style.height = h+2+'px';
            obj.style.width = w+2+'px';
          } else {
            obj.style.height = h+50+'px';
            obj.style.width = w+'px';
          }
        }
      } else {
        // ParenDvi is given
        var myIframe = $(obj).contents().find('#'+key['parentDivId']);
        if(myIframe.length !== 0){
          var h = myIframe.height(),
          w = myIframe.width();

          // Check if border is set solid of with box-siding / inset shadow
          if(key['parentDivSolidBorder'] === true){
            obj.style.height = h+2+'px';
            obj.style.width = w+2+'px';
          } else {
            obj.style.height = h+'px';
            obj.style.width = w+'px';
          }
        }
      }
     });
  };

  window.reloadIframe = function() {
    $('div#main-content div.content iframe')[0].contentWindow.location.reload(true);
  };
</script>
</html>